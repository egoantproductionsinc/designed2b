﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SofaSelect : MonoBehaviour {

    public GameObject particleObject;
    public GameObject[] sofas;
    public Color[] colors;
    public TextMesh dbtx;

    private int sofaIndex;
    private int colorIndex;
    private List<Renderer> sofaRends;

    // Use this for initialization
    void Start () {
        SetDebugText("Started");
        sofaIndex = -1;
        colorIndex = 0;
        SetTextures();
    }

    private void SetTextures()
    {
        sofaRends = new List<Renderer>();
        foreach (GameObject sofa in sofas)
        {
            Renderer r = sofa.transform.Find("MAIN").GetComponent<Renderer>();
            sofaRends.Add(r);
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Swipe("up");
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            Swipe("down");
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Swipe("left");
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            Swipe("right");
        }
    }


    public void SetDebugText(string newText)
    {
        if(dbtx != null)
        {
            dbtx.text = newText;
        }
    }

    public void Swipe(string dir)
    {
        HideSofas();
        SetDebugText(dir);
        switch (dir)
        {
            case "up":
                sofaIndex++;
                break;
            case "down":
                sofaIndex--;
                break;
            case "left":
                colorIndex--;
                break;
            case "right":
                colorIndex++;
                break;
        }

        if(colorIndex <= -1)
        {
            colorIndex = colors.Length - 1;
        }else if (colorIndex > colors.Length - 1)
        {
            colorIndex = 0;
        }

        if(sofaIndex > sofas.Length - 1)
        {
            sofaIndex = -1;
        }else if(sofaIndex < -1)
        {
            sofaIndex = sofas.Length - 1;
        }
        if(sofaIndex > -1)
        {
            sofas[sofaIndex].SetActive(true);
            try
            {
                //sofaRends[sofaIndex].material.SetColor("_MainTex", colors[colorIndex]);
                sofaRends[sofaIndex].material.color = colors[colorIndex];
            }
            catch(Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }
        else
        {
            particleObject.SetActive(true);
        }
    }

    private void HideSofas()
    {
        particleObject.SetActive(false);
        foreach (GameObject sofa in sofas)
        {
            sofa.SetActive(false);
        }
    }
}
