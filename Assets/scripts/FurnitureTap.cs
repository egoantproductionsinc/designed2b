﻿using UnityEngine;
using System.Collections;

public class FurnitureTap : MonoBehaviour {

    public string callFunction = "";
    public GameObject callObject = null;
    public int furnitureIndex;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        if(callObject != null && callFunction != "")
        {
            callObject.SendMessage(callFunction, furnitureIndex);
        }
    }


}
