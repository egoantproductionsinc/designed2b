﻿using UnityEngine;
using System.Collections;

public class SaveBox : MonoBehaviour {

    public int saveSlot;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnMouseDown()
    {
        StateManager rootController = GameObject.Find("/SceneManager").GetComponent<StateManager>();
        rootController.SaveToSlot(saveSlot);
    }


}
