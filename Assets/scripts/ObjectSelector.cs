﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class ObjectSelector : MonoBehaviour {

    public BuildManager buildManager;
    public StateManager stateManager;
    public Text pieceDescription;

    private int activeObj = 0;
    private List<GameObject> validObjs;
    private GameObject SelectionHolder;
    private string activePosition = "";

    // Use this for initialization
    void Start() {

    }

    void Awake()
    {
        DestroyChildren();
    }

    public void DestroyChildren()
    {
        if (SelectionHolder != null)
        {
            foreach (Transform child in SelectionHolder.transform)
            {
                Destroy(child.gameObject);
            }
        }

    }

    public void SetActive(bool isActive)
    {
        transform.gameObject.SetActive(isActive);
    }

    public void SetDirection(string newDir = "")
    {
        activePosition = newDir;
    }

    public void BuildSelectionList(Vector3 placementPosition, string parentID = "", string placementZone = "")
    {
        DestroyChildren();
        SelectionHolder = new GameObject();
        SelectionHolder.name = "SelectionHolder";
        SelectionHolder.transform.parent = transform;
        SelectionHolder.transform.position = placementPosition; //transform.position;
        if(placementZone == "")
        {
            SelectionHolder.transform.Translate(new Vector3(0, 0.3f, 0));
        }
        else if(placementZone == "left") {
            SelectionHolder.transform.Translate(new Vector3(1.25f, 0, 0));
        }
        else if(placementZone == "right"){
            SelectionHolder.transform.Translate(new Vector3(-1.25f, 0, 0));
        }
        

        validObjs = buildManager.GetValidGameObjectList(parentID, activePosition);
        for (var i = 0; i < validObjs.Count; i++)
        {
            validObjs[i].transform.parent = SelectionHolder.transform;
            validObjs[i].transform.position = SelectionHolder.transform.position;
            if (i > 0)
            {
                validObjs[i].SetActive(false);
            } else
            {
                ColorObject(validObjs[i]);
            }
        }
        activeObj = 0;

        SelectionHolder.transform.localScale = new Vector3(1.05f, 1.05f, 1.05f);
        SelectionHolder.transform.Rotate(new Vector3(0, 180, 0));
    }

    private void ShowActiveObject()
    {
        if (activeObj < 0)
        {
            activeObj = validObjs.Count - 1;
        }
        if (activeObj >= validObjs.Count)
        {
            activeObj = 0;
        }
        for (var i = 0; i < validObjs.Count; i++)
        {
            if (i == activeObj)
            {
                validObjs[i].SetActive(true);
                ColorObject(validObjs[i]);

                FurnitureMeta fm = validObjs[i].GetComponent<FurnitureMeta>();
                pieceDescription.text = fm.Description;
            }
            else
            {
                validObjs[i].SetActive(false);
            }
        }

    }

    private void ColorObject(GameObject gameObject)
    {
        buildManager.TintObject(ref gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Swipe("up");
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            Swipe("down");
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Swipe("left");
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            Swipe("right");
        }

        //if ((float)SelectionHolder.transform.localRotation.x > -5f)
        //{
        //    SelectionHolder.transform.Rotate(new Vector3(0.01f, 0, 0));
        //}


    }


    public void SnapObjectInPlace()
    {
        FurnitureMeta fm = validObjs[activeObj].GetComponent<FurnitureMeta>();
        buildManager.AddFurniturePiece(fm.PieceID, activePosition);
        stateManager.AddBuiltObject();
        transform.gameObject.SetActive(false);
    }


    public void Swipe(string dir)
    {
        switch (dir)
        {
            case "up":
                activeObj++;
                ShowActiveObject();
                break;
            case "down":
                activeObj--;
                ShowActiveObject();
                break;
            case "left":
                buildManager.SendMessage("adjustColour",-1);
                ShowActiveObject();
                break;
            case "right":
                buildManager.SendMessage("adjustColour", 1);
                ShowActiveObject();
                break;
        }

    }

}
