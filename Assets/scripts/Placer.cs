﻿using UnityEngine;
using System.Collections;

public class Placer : MonoBehaviour {

    public bool isStartingObject;

    private string placementPosition = "";
    private string placementParentID = "";

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetPlacementParentID(string parentID)
    {
        placementParentID = parentID;
    }

    public void SetPlacementPosition(string newPos)
    {
        placementPosition = newPos;
    }

    public void OnMouseUp()
    {
        StateManager rootController = GameObject.Find("/SceneManager").GetComponent<StateManager>();
        switch (placementPosition)
        {
            case "":
                rootController.AddObjectTo("", transform.position, "");
                transform.gameObject.SetActive(false);
                break;
            case "right":
                rootController.AddObjectTo(placementPosition, transform.position, placementParentID );
                transform.gameObject.SetActive(false);
                break;
            case "left":
                rootController.AddObjectTo(placementPosition, transform.position, placementParentID);
                transform.gameObject.SetActive(false);
                break;
        }

    }


}
