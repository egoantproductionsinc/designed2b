﻿using UnityEngine;
using System.Collections;

public class FurnitureMeta : MonoBehaviour {

    public string GroupName;
    public string PieceID;
    public string Description;
    public bool isStartingPiece;
    public bool canBeFlipped;

    public Anchor leftAnchor;
    public Anchor rightAnchor;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [System.Serializable]
    public class Anchor
    {
        public bool isUsed;
        public Vector3 offsetPosition;
        public Vector3 childRotation;
    }


}
