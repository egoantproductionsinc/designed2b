﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class BuildManager : MonoBehaviour {

    public GameObject[] FurniturePrefabs;
    public GameObject originalPlacer;
    public GameObject placerPrefabRight;
    public GameObject placerPrefabLeft;
    public StateManager stateManager;
    public int maxObjects = 4;
    public float ARFurnitureScale;
    public float ARSeatHeight;

    [Header("Color Management")]
    public int colorIndex = 0;
    public int colorGroupIndex = 0;
    public FurnitureColor[] colors;
    public FurnitureTextureGroup[] colorGroups;

    
    private List<FurnitureMeta> activeBuild;
    private string buildModeType = "";
    private string ARmode = "floor";

    [Header("Buttons")]
    public GameObject SaveButton;
    public GameObject LoadButton;

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Awake()
    {

    }

    public void SetBuildModeType(string newType)
    {
        //TODO: Add a check to see if the type exists in the list of valid objects
        originalPlacer.SetActive(true);
        buildModeType = newType;
        if(newType == "")
        {
            activeBuild = new List<FurnitureMeta>();
        }
    }

    public void adjustColour(int adjBy)
    {

        colorIndex += adjBy;
        if(colorIndex < 0)
        {
            colorGroupIndex -= 1;
            if (colorGroupIndex < 0)
            {
                colorGroupIndex = colorGroups.Length - 1;

            }
            colorIndex = colorGroups[colorGroupIndex].Colors.Length - 1;
            //colorIndex = colors.Length - 1;
        }
        if (colorIndex > colorGroups[colorGroupIndex].Colors.Length - 1)
        {
            colorIndex = 0;
            colorGroupIndex++;
            if(colorGroupIndex > colorGroups.Length - 1)
            {
                colorGroupIndex = 0;
            }
        }
    }

    public void setColour(int colorGroup, int newColour)
    {
        colorIndex = newColour;
        colorGroupIndex = colorGroup;
        if (colorIndex < 0)
        {
            colorIndex = colors.Length - 1;
        }
        if (colorIndex > colors.Length - 1)
        {
            colorIndex = 0;
        }
    }

    public void TintObject(ref GameObject objectToColor)
    {
        for (var c = 0; c < objectToColor.transform.childCount; c++)
        {
            GameObject sub = objectToColor.transform.GetChild(c).gameObject;
            if (sub.name.ToLower().Contains("_main"))
            {
                Renderer r = sub.GetComponent<Renderer>();
                r.material.mainTexture = colorGroups[colorGroupIndex].Texture;
                //r.material.SetTexture(Shader.PropertyToID("Albedo"), colorGroups[colorGroupIndex].Texture);
                r.material.color = colorGroups[colorGroupIndex].Colors[colorIndex].Color;
            }
        }

    }

    private FurnitureMeta GetPieceMetaByID(string pieceID)
    {
        FurnitureMeta selectedFM = null;
        for (int i = 0; i < FurniturePrefabs.Length; i++)
        {
            FurnitureMeta fm = FurniturePrefabs[i].GetComponent<FurnitureMeta>();
            if (fm.PieceID == pieceID)
            {
                selectedFM = fm;
                break;
            }
        }
        return selectedFM;
    }

    private GameObject GetPiecePrefabByID(string pieceID)
    {
        GameObject selectedGO = null;
        for (int i = 0; i < FurniturePrefabs.Length; i++)
        {
            FurnitureMeta fm = FurniturePrefabs[i].GetComponent<FurnitureMeta>();
            if (fm.PieceID == pieceID)
            {
                selectedGO = FurniturePrefabs[i];
                break;
            }
        }
        return selectedGO;
    }

    public string GetCurrentConfiguration()
    {
        string buildOut = "";
        if (activeBuild.Count > 0)
        {
            buildOut = colorGroupIndex + ",";
            buildOut += colorIndex + ",";
            for (var i = 0; i < activeBuild.Count; i++)
            {
                buildOut += activeBuild[i].PieceID;
                if (i < activeBuild.Count - 1)
                {
                    buildOut += ",";
                }
            }
        }

        return buildOut;
    }

    public void LoadConfigurationFromString(string inConfig)
    {
        string[] config = inConfig.Split(',');
        setColour(System.Convert.ToInt32(config[0]), System.Convert.ToInt32(config[1]));
        AddFurniturePiece(config[2]);
        for(var i = 3; i < config.Length - 1; i++)
        {
            AddFurniturePiece(config[i],"right");
        }

    }


    public List<string> GetValidObjects(string parentID)
    {
        //Returns a list of all the valid objects based on the input object

        //Find the identified object
        FurnitureMeta selectedFM = GetPieceMetaByID(parentID);

        //Build a list of all valid pieces.
        List<string> validList = new List<string>();
        if (selectedFM != null)
        {
            if (selectedFM.leftAnchor.isUsed == true || selectedFM.rightAnchor.isUsed)
            {
                for (int s = 0; s < FurniturePrefabs.Length; s++)
                {
                    FurnitureMeta fm = FurniturePrefabs[s].GetComponent<FurnitureMeta>();
                    //Check to see if the item is from a compatable group
                    if (selectedFM.GroupName == fm.GroupName)
                    {
                        if (selectedFM.leftAnchor.isUsed && fm.rightAnchor.isUsed || selectedFM.rightAnchor.isUsed && fm.leftAnchor.isUsed)
                            //|| ((selectedFM.leftAnchor.isUsed && fm.leftAnchor.isUsed || selectedFM.rightAnchor.isUsed && fm.rightAnchor.isUsed) && fm.canBeFlipped))
                        {
                            validList.Add(fm.PieceID);
                        }
                    }

                }
            }
        }

        return validList;
    }

    public List<GameObject> GetValidGameObjectList(string parentID = null, string activePosition = "")
    {
        //Returns a list of all the valid objects based on the input object

        //Find the identified object

        FurnitureMeta selectedFM = null;
        if (parentID != null && parentID != "")
        {
            selectedFM = GetPieceMetaByID(parentID);
        }

        //Build a list of all valid pieces.
        List<GameObject> validList = new List<GameObject>();
        if (selectedFM != null)
        {
            if (selectedFM.leftAnchor.isUsed || selectedFM.rightAnchor.isUsed)
            {
                for (int s = 0; s < FurniturePrefabs.Length; s++)
                {
                    FurnitureMeta fm = FurniturePrefabs[s].GetComponent<FurnitureMeta>();
                    //Check to see if the item is from a compatable group
                    if (selectedFM.GroupName == fm.GroupName)
                    {
                        if (( selectedFM.leftAnchor.isUsed && fm.rightAnchor.isUsed && activePosition == "left") || (selectedFM.rightAnchor.isUsed && fm.leftAnchor.isUsed && activePosition == "right"))
                        {
                            validList.Add(Instantiate<GameObject>(FurniturePrefabs[s]));
                        }
                    }

                }
            }
        }
        else
        {
            //Return a list of valid starter objects
            for (int s = 0; s < FurniturePrefabs.Length; s++)
            {
                FurnitureMeta fm = FurniturePrefabs[s].GetComponent<FurnitureMeta>();
                if (fm.isStartingPiece == true && fm.GroupName.ToLower() == buildModeType)
                {
                    validList.Add(Instantiate<GameObject>(FurniturePrefabs[s]));
                }
            }
        }

        return validList;
    }

    public void AddFurniturePiece(string pieceID, string position = null)
    {
        FurnitureMeta selectedFM = GetPieceMetaByID(pieceID);
        if (selectedFM != null)
        {
            if (position == null || position == "")
            {
                activeBuild = new List<FurnitureMeta>();
                activeBuild.Add(selectedFM);
            }else if (position == "left")
            {
                activeBuild.Insert(0, selectedFM);
            }
            else if (position == "right")
            {
                activeBuild.Add(selectedFM);
            }
        }
    }

    public void RemoveFurnitureAtIndex(int furnIndex)
    {
        List<FurnitureMeta> newBuild = new List<FurnitureMeta>();
        for(int i = 0; i < activeBuild.Count; i++)
        {
            if(i != furnIndex)
            {
                newBuild.Add(activeBuild[i]);
            }
        }

        activeBuild = newBuild;
        stateManager.AddBuiltObject();
    }

    public GameObject GetActiveBuildGameObject(bool addPlacers = false)
    {
        GameObject gmo = new GameObject();
        gmo.name = "FurnitureConstruct";

        GameObject innerHolder = new GameObject();
        innerHolder.name = "inner";

        innerHolder.transform.parent = gmo.transform;
        innerHolder.transform.localPosition = new Vector3(0, 0, 0);

        GameObject prevPiece = null;
        FurnitureMeta prevFM = null;
        float totalMove = 0;

        if(activeBuild.Count > 0)
        {
            for (var i = 0; i < activeBuild.Count; i++)
            {
                GameObject addPiece = GetPiecePrefabByID(activeBuild[i].PieceID);
                FurnitureMeta newFM = GetPieceMetaByID(activeBuild[i].PieceID);
                if (addPiece != null)
                {
                    GameObject newPiece = Instantiate<GameObject>(addPiece);
                    AddTapToObject(ref newPiece, transform.gameObject, i);

                    if (prevPiece == null)
                    {
                        newPiece.transform.parent = innerHolder.transform;
                    }
                    else
                    {
                        newPiece.transform.parent = prevPiece.transform;
                    }

                    TintObject(ref newPiece);

                    if (i > 0)
                    {
                        newPiece.transform.localPosition = new Vector3(0, 0, 0);
                        newPiece.transform.Translate(prevFM.rightAnchor.offsetPosition);
                        newPiece.transform.Rotate(prevFM.rightAnchor.childRotation);
                        newPiece.transform.Translate(newFM.leftAnchor.offsetPosition * -1);
                        totalMove += prevFM.rightAnchor.offsetPosition.x;
                    }
                    else
                    {
                        newPiece.transform.localPosition = new Vector3(0, 0, 0);
                        newPiece.transform.localRotation = new Quaternion(0, 0, 0, 0);
                    }
                    if (addPlacers && activeBuild.Count < maxObjects)
                    {
                        originalPlacer.SetActive(false);
                        //SetPlacementPosition
                        if (newFM.rightAnchor.isUsed && i == activeBuild.Count - 1)
                        {
                            GameObject rightPlacer = Instantiate<GameObject>(placerPrefabLeft);
                            rightPlacer.transform.parent = newPiece.transform;
                            rightPlacer.transform.localPosition = new Vector3(0, 0, 0);
                            rightPlacer.transform.Translate(newFM.rightAnchor.offsetPosition);
                            rightPlacer.SendMessage("SetPlacementPosition", "right");
                            rightPlacer.SendMessage("SetPlacementParentID", newFM.PieceID);
                        }
                        if (newFM.leftAnchor.isUsed && i == 0)
                        {
                            GameObject leftPlacer = Instantiate<GameObject>(placerPrefabRight);
                            leftPlacer.transform.parent = newPiece.transform;
                            leftPlacer.transform.localPosition = new Vector3(0, 0, 0);
                            leftPlacer.transform.Translate(newFM.leftAnchor.offsetPosition);
                            leftPlacer.SendMessage("SetPlacementPosition", "left");
                            leftPlacer.SendMessage("SetPlacementParentID", newFM.PieceID);
                        }
                    }

                    prevPiece = newPiece;
                    prevFM = newFM;
                }
            }

            innerHolder.transform.Translate(new Vector3(0 - totalMove, 0, 0));
        }
        else
        {
            if (addPlacers)
            {
                originalPlacer.SetActive(true);
            }
        }

        return gmo;

    }

    public void SofaMode()
    {
        ARmode = "sofa";
    }

    public void FloorMode()
    {
        ARmode = "floor";
    }

    public GameObject GetActiveRawGameObject()
    {
        GameObject gmo = new GameObject();
        gmo.name = "FurnitureConstruct";

        GameObject innerHolder = new GameObject();
        innerHolder.name = "inner";

        innerHolder.transform.parent = gmo.transform;
        innerHolder.transform.localPosition = new Vector3(0, 0, 0);
       

        GameObject prevPiece = null;
        FurnitureMeta prevFM = null;
        float totalMove = 0;

        if (activeBuild.Count > 0)
        {
            for (var i = 0; i < activeBuild.Count; i++)
            {
                GameObject addPiece = GetPiecePrefabByID(activeBuild[i].PieceID);
                FurnitureMeta newFM = GetPieceMetaByID(activeBuild[i].PieceID);
                if (addPiece != null)
                {
                    GameObject newPiece = Instantiate<GameObject>(addPiece);
                    
                    if (prevPiece == null)
                    {
                        newPiece.transform.parent = innerHolder.transform;
                    }
                    else
                    {
                        newPiece.transform.parent = prevPiece.transform;
                    }

                    TintObject(ref newPiece);

                    if (i > 0)
                    {
                        newPiece.transform.localPosition = new Vector3(0, 0, 0);
                        newPiece.transform.Translate(prevFM.rightAnchor.offsetPosition);
                        newPiece.transform.Rotate(prevFM.rightAnchor.childRotation);
                        newPiece.transform.Translate(newFM.leftAnchor.offsetPosition * -1);
                        totalMove += prevFM.rightAnchor.offsetPosition.x;
                    }
                    else
                    {
                        newPiece.transform.localPosition = new Vector3(0, 0, 0);
                        newPiece.transform.localRotation = new Quaternion(0, 0, 0, 0);
                    }

                    prevPiece = newPiece;
                    prevFM = newFM;
                }
            }

            innerHolder.transform.Translate(new Vector3(0 - totalMove, 0, 0));
            innerHolder.transform.localScale = new Vector3(ARFurnitureScale, ARFurnitureScale, ARFurnitureScale);
            if (ARmode == "sofa")
            {
                Debug.Log("Sofa mode.");
                innerHolder.transform.Translate(new Vector3(0, 0 - ARSeatHeight, 0));
            }
        }

        return gmo;

    }

    public void AddTapToObject(ref GameObject gameObj, GameObject whoYaGonnaCall, int furnitureIndex)
    {
        for (var c = 0; c < gameObj.transform.childCount; c++)
        {
            GameObject sub = gameObj.transform.GetChild(c).gameObject;
            if (sub.name.ToLower().Contains("_main"))
            {

                sub.AddComponent<MeshCollider>();
                sub.AddComponent<FurnitureTap>();

                FurnitureTap f = sub.GetComponent<FurnitureTap>();
                f.callFunction = "RemoveFurnitureAtIndex";
                f.callObject = whoYaGonnaCall;
                f.furnitureIndex = furnitureIndex;
            }
        }

    }

    [System.Serializable]
    public class FurnitureTextureGroup
    {
        public string GroupName;
        public Texture2D Texture;
        public FurnitureColor[] Colors;
    }

    [System.Serializable]
    public class FurnitureColor
    {
        public string Name;
        public Color Color;
    }

}
