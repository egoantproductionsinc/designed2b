﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class StateManager : MonoBehaviour {

    public GameObject MainMenu;
    
    public GameObject ARScene;
    public GameObject LoadSaveMain;
    public GameObject SaveWindow;
    public GameObject LoadWindow;
    public RenderTexture saveRenderTexture;

    public GameObject baseBuildObject;
    public BuildManager buildManager;
    public ObjectSelector pieceSelector;

    public Image[] SaveImages;
    public Image[] LoadImages;
    public GameObject[] LoadFieldObjects;

    [Header("Create UI")]
    public GameObject CreationRoom;
    public GameObject UITypeSelect;
    public GameObject UIBuildInterface;
    public BoxCollider selectionCollider;

    [Header("Camera Management")]
    public GameObject CameraAugmented;
    public GameObject Camera2D;
    public GameObject Camera2DFuture;
    public float cameraLerpSpeed;
    private Quaternion OriginalCamera2D;
    private Quaternion FromRotation;

    // Use this for initialization
    void Start () {
        ChangeState("MainMenu");
        Debug.Log("App path: " + Application.persistentDataPath);
        OriginalCamera2D = Camera2D.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (savingToSlot > 0 && isSaving == true)
        {
            isSaving = false;
            StartCoroutine("SavetoFile");
        }
        if(Camera2D.transform.rotation != Camera2DFuture.transform.rotation)
        {
            Camera2D.transform.rotation = Quaternion.Lerp(Camera2D.transform.rotation, Camera2DFuture.transform.rotation, Time.deltaTime * cameraLerpSpeed);
        }
    }

    public void ChangeState(string newState)
    {
        switch (newState)
        {
            case "MainMenu":
                MainMenu.SetActive(true);
                CreationRoom.SetActive(false);
                ARScene.SetActive(false);
                Camera2D.SetActive(true);
                CameraAugmented.SetActive(false);
                break;
            case "CreationRoom":
                MainMenu.SetActive(false);
                CreationRoom.SetActive(true);
                ARScene.SetActive(false);
                Camera2D.SetActive(true);
                CameraAugmented.SetActive(false);
                break;
            case "ARScene":
                MainMenu.SetActive(false);
                CreationRoom.SetActive(false);
                ARScene.SetActive(true);
                buildManager.SendMessage("FloorMode");
                Camera2D.SetActive(false);
                CameraAugmented.SetActive(true);
                break;

            case "ARSceneReplace":
                MainMenu.SetActive(false);
                CreationRoom.SetActive(false);
                ARScene.SetActive(true);
                buildManager.SendMessage("SofaMode");
                Camera2D.SetActive(false);
                CameraAugmented.SetActive(true);
                break;
        }
    }

    public void AddBuiltObject()
    {
        for (int i = 0; i < baseBuildObject.transform.childCount; i++)
        {
            Destroy(baseBuildObject.transform.GetChild(0).gameObject);
        }

        GameObject builtObj = buildManager.GetActiveBuildGameObject(true);
        builtObj.transform.parent = baseBuildObject.transform;
        builtObj.transform.localPosition = new Vector3(0, 0, 0);
        builtObj.transform.Rotate(0, 180, 0);
        LoadSaveMain.SetActive(true);
        ResetCameraToStart();
    }

    public void AddObjectTo(string newPosition, Vector3 placementPosition, string pieceID = "")
    {
        Debug.Log("Show window for " + newPosition + " piece");
        LoadSaveMain.SetActive(false);
        pieceSelector.SetActive(true);
        SetColliderActive(true);
        if (newPosition == "left" || newPosition == "right")
        {
            pieceSelector.SetDirection(newPosition);
            pieceSelector.BuildSelectionList(placementPosition, pieceID, newPosition);
            FromRotation = Camera2D.transform.rotation;
            Camera2DFuture.transform.LookAt(placementPosition);
        }
        else
        {
            pieceSelector.SetDirection("");
            pieceSelector.BuildSelectionList(placementPosition);

        }

    }

    public void SetColliderActive(bool isActive)
    {
        selectionCollider.enabled = isActive;
    }

    public void ResetCameraToStart()
    {
        Camera2DFuture.transform.rotation = OriginalCamera2D;
    }


    private Vector3 startTapPos;
    public void OnMouseDown()
    {
        startTapPos = Input.mousePosition;
    }

    public void OnMouseUp()
    {
        Debug.Log("In mid zone: " + IsInMiddleZone(Input.mousePosition));
        if (Vector3.Distance(startTapPos, Input.mousePosition) < 40 && IsInMiddleZone(Input.mousePosition))
        {
            pieceSelector.SnapObjectInPlace();
            selectionCollider.enabled = false;
        }
    }

    private bool IsInMiddleZone(Vector3 mousePosition)
    {
        bool inZone = false;
        Rect screenR = new Rect(new Vector2(Screen.width * 0.2f, Screen.height * 0.2f), new Vector2(Screen.width * 0.6f, Screen.height * 0.6f));
        inZone = screenR.Contains(mousePosition);

        return inZone;
    }


    private int savingToSlot = 0;
    private bool isSaving = false;
    public void SaveToSlot(int slotNum)
    {
        savingToSlot = slotNum;
        isSaving = true;
    }

    public void LoadFromSlot(int slotNum)
    {
        buildManager.LoadConfigurationFromString(ES2.Load<string>(Application.persistentDataPath + "/save" + slotNum + ".txt"));
        AddBuiltObject();
        LoadSaveMain.SetActive(true);
        LoadWindow.SetActive(false);
        SaveWindow.SetActive(false);
    }

    IEnumerator SavetoFile()
    {

        RenderTexture currentActiveRT = RenderTexture.active;
        Texture2D textureOut = new Texture2D(1920, 1080);
        RenderTexture.active = saveRenderTexture;
        Rect spriteRect = new Rect(0, 0, saveRenderTexture.width, saveRenderTexture.height);
        textureOut.ReadPixels(spriteRect, 0, 0);
        textureOut.Apply();
        ES2.SaveImage(textureOut, Application.persistentDataPath + "/save" + savingToSlot + ".png");
        
        RenderTexture.active = currentActiveRT;
        Sprite saveSprite = Sprite.Create(textureOut, spriteRect, new Vector2(0,0));
        SaveImages[savingToSlot - 1].sprite = saveSprite;

        ES2.Save<string>(buildManager.GetCurrentConfiguration(), Application.persistentDataPath + "/save" + savingToSlot + ".txt");
        Debug.Log(buildManager.GetCurrentConfiguration());

        //string whatsinthere = ES2.Load<string>(Application.persistentDataPath + "/save" + savingToSlot + ".txt");
        //Debug.Log("===[" + whatsinthere + "]===");

        savingToSlot = 0;


        yield return null;
    }

    IEnumerator GetLoadImages()
    {
        //Check to see which save files are present
        for (int i = 0; i < 6; i++)
        {
            string imgPath= Application.persistentDataPath + "/save" + (i + 1) + ".png";
            if (ES2.Exists(imgPath))
            {
                LoadImages[i].sprite = Sprite.Create(
                    ES2.LoadImage(imgPath),
                    new Rect(new Vector2(0, 0), new Vector2(1920, 1080)),
                    new Vector2(0, 0)
                    );

            }else
            {
                LoadFieldObjects[i].SetActive(false);
            }
        }
        yield return null;
    }


    IEnumerator GetSaveImages()
    {
        //Check to see which save files are present
        for (int i = 0; i < 6; i++)
        {
            string imgPath = Application.persistentDataPath + "/save" + (i + 1) + ".png";
            if (ES2.Exists(imgPath))
            {
                SaveImages[i].sprite = Sprite.Create(
                    ES2.LoadImage(imgPath),
                    new Rect(new Vector2(0, 0), new Vector2(1920, 1080)),
                    new Vector2(0, 0)
                    );
            }
        }
        yield return null;
    }

    public void OpenSaveState()
    {
        LoadSaveMain.SetActive(false);
        LoadWindow.SetActive(false);
        SaveWindow.SetActive(true);

        StartCoroutine("GetSaveImages");

    }

    public void OpenLoadState()
    {
        LoadSaveMain.SetActive(false);
        LoadWindow.SetActive(true);
        SaveWindow.SetActive(false);

        StartCoroutine("GetLoadImages");

    }

    public void ClearBuildType()
    {
        buildManager.SetBuildModeType("");
        UITypeSelect.SetActive(true);
        UIBuildInterface.SetActive(false);
        AddBuiltObject();
    }


    public void SelectDAX()
    {
        buildManager.SetBuildModeType("dax");
        UITypeSelect.SetActive(false);
        UIBuildInterface.SetActive(true);
    }

    public void SelectDEZ()
    {
        buildManager.SetBuildModeType("dez");
        UITypeSelect.SetActive(false);
        UIBuildInterface.SetActive(true);
    }

    public void SelectDOV()
    {
        buildManager.SetBuildModeType("dov");
        UITypeSelect.SetActive(false);
        UIBuildInterface.SetActive(true);
    }


}
