﻿using UnityEngine;
using System.Collections;

public class ClickHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("trying tap");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.Log("tap found");
                if (hit.transform.gameObject.tag == "tappable")
                {
                    Debug.Log("sending message");
                    hit.transform.gameObject.SendMessage("Tapped");
                }
            }
        }
    }


}
