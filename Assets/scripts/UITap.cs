﻿using UnityEngine;
using System.Collections;

public class UITap : MonoBehaviour {

    public GameObject objectToCall;
    public string functionName;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnMouseUp()
    {
        objectToCall.SendMessage(functionName);
    }


}
