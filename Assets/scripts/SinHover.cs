﻿using UnityEngine;
using System.Collections;

public class SinHover : MonoBehaviour {

    public float vertFloat;
    public float floatSpeed;
    private float currentCycle;
    private Vector3 initPos;


	// Use this for initialization
	void Start () {
        currentCycle = 0;
    }

    void Awake()
    {
        initPos = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
            currentCycle += floatSpeed * Time.deltaTime;
            float sinY = Mathf.Sin(currentCycle) * vertFloat;
            transform.localPosition = new Vector3(initPos.x, initPos.y + sinY, initPos.z);
    }
}
