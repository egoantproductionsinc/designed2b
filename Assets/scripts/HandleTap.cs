﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HandleTap : MonoBehaviour, IPointerClickHandler
{

    public GameObject[] toggleItems;
    public GameObject defaultObject;
    public GameObject myObject;

    private bool iAmActive = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void DisableAll()
    {
        foreach (GameObject g in toggleItems)
        {
            g.SetActive(false);
        }
    }

    private void ActivateDefault()
    {
        defaultObject.SetActive(true);
    }

    public void Tapped()
    {
        Debug.Log("tapped " + gameObject.name);
        if (iAmActive)
        {
            DisableAll();
            ActivateDefault();
        }
        else
        {
            DisableAll();
            myObject.SetActive(true);
        }
        iAmActive = !iAmActive;
    }

    public void OnMouseUp()
    {
        Tapped();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Tapped();
    }

}
