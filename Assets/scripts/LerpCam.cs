﻿using UnityEngine;
using System.Collections;

public class LerpCam : MonoBehaviour {

    public GameObject targetCam;
    public GameObject lookTarget;

    private Vector3 speed = Vector3.zero;

    // Use this for initialization
    void Start () {
        transform.position = targetCam.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.SmoothDamp(transform.position, targetCam.transform.position, ref speed, 0.4f);
        transform.LookAt(lookTarget.transform);
    }
}
