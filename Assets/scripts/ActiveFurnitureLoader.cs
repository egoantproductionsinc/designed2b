﻿using UnityEngine;
using System.Collections;

public class ActiveFurnitureLoader : MonoBehaviour {

    public BuildManager buildManager;

    public Vector3 initialRotation;
    public Vector3 initialPosition;
    public Vector3 initialScale;

    void OnEnable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(0).gameObject);
        }

        GameObject builtObj = buildManager.GetActiveRawGameObject();
        builtObj.transform.parent = transform;
        builtObj.transform.localPosition = initialPosition;
        builtObj.transform.Rotate(initialRotation.x, initialRotation.y, initialRotation.z);
        builtObj.transform.localScale = initialScale;
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
